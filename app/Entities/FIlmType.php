<?php

namespace App\FilmType;

class FIlmType
{
    const STORY = 1;
    const ANIMATION = 2;
    const DOCUMENTARY = 3;
    const SCIENCE = 4;
    const SPECIAL = 5;
    const COOPERATION = 6;

    public function getTypes() {
        return [
            self::STORY       => '故事影片',
            self::ANIMATION   => '动画影片',
            self::DOCUMENTARY => '纪录影片',
            self::SCIENCE     => '科教影片',
            self::SPECIAL     => '特种影片',
            self::COOPERATION => '合拍片'
        ];
    }
}
