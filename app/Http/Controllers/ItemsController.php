<?php

namespace App\Http\Controllers;

use App\Repositories\FilmRepository;
use App\Repositories\ItemRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Goutte\Client;

class ItemsController extends Controller
{
    protected $client;

    protected $itemRepository;

    protected $filmRepository;

    protected $hostname;

    public function __construct(ItemRepository $itemRepository, FilmRepository $filmRepository)
    {
        $this->client = new Client();
        $this->itemRepository = $itemRepository;
        $this->filmRepository = $filmRepository;
        $this->hostname = 'http://' . parse_url(env('APP_FETCH_URL'))['host'];
    }

    public function index()
    {
        $data = $this->fetchIndex();

        $items_in_db = $this->itemRepository->pluck('item_id');
        $diff = collect($data)->keys()->diff($items_in_db);

        // 是否有新记录
        if ($diff->isEmpty()) {
            return response()->json(['status' => 1]);
        }

        $diff->each(function ($item) use ($data) {
            $this->itemRepository->create($data[$item]);

            // 抓取新记录
            $this->fetchList($data[$item]);
        });

        // 抓取详细信息
        $this->fetchDetail();

        return response()->json(['status' => 1]);
    }

    private function fetchIndex()
    {
        $crawler = $this->client->request('GET', env('APP_FETCH_URL'));

        $raw = $crawler->filter('ul[style]')->each(function ($node) {
            $url = $this->hostname . $node->filter('a')->attr('href');
            $query = [];
            parse_str(parse_url($url)['query'], $query);

            return [
                'item_id' => $query['id'],
                'url' => $url,
                'title' => $node->filter('a')->text(),
                'published_at' => Carbon::createFromFormat('Y-m-d', trim($node->filter('li')->eq(1)->text()))
            ];
        });

        foreach ($raw as $item) {
            $data[$item['item_id']] = $item;
        }

        return $data;
    }

    private function fetchList(array $data)
    {
        $crawler = $this->client->request('GET', $data['url']);

        $this->itemRepository->update($data['item_id'], [
            'title' => $crawler->filter('div.heading')->text(),
            'content' => join('', $crawler->filter('p')->each(function ($node) {
                return ltrim($node->text());
            }))
        ]);

        for($type = 1; $type <= 6; $type++) {
             $raw = $crawler->filter("#divf{$type} tr")->each(function ($node, $index) use ($data, $type) {
                 if (! $index) {
                     return [];
                 }

                 $temp = array_combine(
                     ['url', 'license', 'title', 'company', 'screen_writer', 'result', 'address'],
                     $node->filter('td')->each(function($node) {
                         return $node->text();
                     })
                 );

                 $temp['item_id'] = $data['item_id'];
                 $temp['url'] = $this->hostname . substr($node->filter('td a')->attr('onclick'), 24, 108);
                 $temp['type'] = $type;
                 $temp['created_at'] = Carbon::now();
                 $temp['updated_at'] = Carbon::now();

                 return $temp;
             });

            array_shift($raw);
            $this->filmRepository->batchInsert($raw);
        }

        return true;
    }

    private function fetchDetail()
    {
        $films = $this->filmRepository->getNullContentFilms();

        if ($films->isEmpty()) {
            return false;
        }

        $films->each(function ($film) {
            $crawler = $this->client->request('GET', $film->url);

            $film->update([
                'content' => trim($crawler->filter("tr[valign]")->text())
            ]);
        });

        return true;
    }
}
