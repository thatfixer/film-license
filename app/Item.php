<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = ['item_id', 'title', 'content', 'url', 'published_at'];
}
