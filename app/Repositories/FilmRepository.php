<?php

namespace App\Repositories;
use App\Film;
use Illuminate\Support\Facades\DB;

class FilmRepository
{
    public function batchInsert(array $attributes)
    {
        return DB::table('films')->insert($attributes);
    }

    public function getNullContentFilms()
    {
        return Film::whereRaw('content is null')->get();
    }
}
