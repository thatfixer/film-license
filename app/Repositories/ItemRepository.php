<?php

namespace App\Repositories;
use App\Item;

class ItemRepository
{
    public function pluck($column)
    {
        return Item::pluck($column);
    }

    public function create(array $attributes)
    {
        return Item::create($attributes);
    }

    public function update($item_id, array $attribute)
    {
        $item = Item::where('item_id', $item_id)->first();

        $item->update($attribute);

        return $item;
    }
}
