<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_id');
            $table->string('title'); // 片名
            $table->tinyInteger('type'); // 类型
            $table->text('content')->nullable(); // 梗概
            $table->string('license'); // 备案立项号
            $table->string('company'); // 备案单位
            $table->string('address');  // 备案地
            $table->string('screen_writer'); // 编剧
            $table->string('result'); // 备案结果
            $table->string('url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}
